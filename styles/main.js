function navSlide() {
    const burger = document.querySelector(".burger");
    const nav = document.querySelector(".nav-links");
    const navLinks = document.querySelectorAll(".nav-links li");
    
    burger.addEventListener("click", () => {
        //Toggle Nav
        nav.classList.toggle("nav-active");
        
        //Animate Links
        navLinks.forEach((link, index) => {
            if (link.style.animation) {
                link.style.animation = ""
            } else {
                link.style.animation = `navLinkFade 0.5s ease forwards ${index / 7 + 0.5}s`;
            }
        });
        //Burger Animation
        burger.classList.toggle("toggle");
    });
    
}

navSlide();

//Quick form
const form = document.querySelector('#myForm');


async function handleSubmit(event) {
    event.preventDefault();
    const status = document.querySelector("#status");
    var data = new FormData(event.target);
    fetch(event.target.action, {
      method: form.method,
      body: data,
      headers: {
          'Accept': 'application/json'
      }
    }).then(response => {
      status.innerHTML = "Thanks for your submission!";
      form.reset()
    }).catch(error => {
      status.innerHTML = "Oops! There was a problem submitting your form"
    });
  }
  form.addEventListener("submit", handleSubmit);

  //added





$(document).ready(function(){
  $('.slider').slick({
   arrows:false,
   dots: true,
   appendDots:'.slider-dots',
   dotClass:'dots',
  });

  
  let burger = document.querySelector('.burger');
  let times = document.querySelector('.times');
  let mobileNav = document.querySelector('.mobile-nav')
  burger.addEventListener('click', function () {
      mobileNav.classList.add('open');
     
  })
  times.addEventListener('click', function () {
      mobileNav.classList.remove('open');
  })
})
